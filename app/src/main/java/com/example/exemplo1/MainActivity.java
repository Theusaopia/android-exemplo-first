package com.example.exemplo1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.DeadObjectException;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText edNum;
    TextView tvResult;
    Double total = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edNum = (EditText) findViewById(R.id.edNumber);
        tvResult = (TextView) findViewById(R.id.textResult);
    }

    public void somar(View view)
    {
        try
        {
            Double value = Double.parseDouble(edNum.getText().toString());
            total += value;
            edNum.setText("");
            tvResult.setText(""+total);
        }
        catch (NumberFormatException nfe)
        {
            Toast.makeText(this, "Please input a valid number", Toast.LENGTH_SHORT).show();
        }
    }
}